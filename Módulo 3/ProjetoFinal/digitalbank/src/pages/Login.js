import React from 'react';
import '../css/login.css';
import Input from '../components/input';
import { Link } from 'react-router-dom';

const Login = () =>
    <div className="login">
        <div className="login-triangle"></div>

        <h2 className="login-header">Log in</h2>

        <form class="login-container">
            <Input type="text" placeholder="Email"/>
            <Input type="text" placeholder="Senha"/>
            <Link to="/Home"><p><button>Log in</button></p></Link>
        </form>
    </div >


export default Login;