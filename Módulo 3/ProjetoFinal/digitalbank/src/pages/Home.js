import React from 'react';
import { Link } from 'react-router-dom';
import '../css/paginas.css'

function Home() {
    return(
        <div>
            <section className="section">
                <h1 className="text">Digital Bank - Home</h1>
            </section>
            <section className="section2">
                <Link to="/Agencias"><p><a>Agencias</a></p></Link>
                <Link to="/Clientes"><p><a>Clientes</a></p></Link>
                <Link to="/TiposDeContas"><p><a>Tipos de contas</a></p></Link>
                <Link to="/ContasDeClientes"><p><a>Contas de clientes</a></p></Link>
            </section>
        </div>
    );
}

export default Home;