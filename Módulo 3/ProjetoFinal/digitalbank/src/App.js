import React, { Component }from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Login from './pages/Login'
import Home from './pages/Home'
import Agencias from './pages/Agencias' 
import Clientes from './pages/Clientes'
import TiposDeContas from './pages/TiposDeContas'
import ContasDeClientes from './pages/ContasDeClientes'

function App() {
  return (
    <Router>
        <Route exact path='/' component = { Login }/>
        <Route path='/Home' component = { Home }/>
        <Route path='/Agencias' component = { Agencias }/>
        <Route path='/Clientes' component = { Clientes }/>
        <Route path='/TiposDeContas' component = { TiposDeContas }/>
        <Route path='/ContasDeClientes' component = { ContasDeClientes }/>
    </Router>
  );
}

export default App;
