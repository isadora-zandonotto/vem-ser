import React, { Component } from 'react'

const Input = props => {
    return (
        <React.Fragment>
        <p><input type={props.type} placeholder={props.placeholder} /></p>
        </React.Fragment>
    )

}

export default Input;