import React from 'react'

const CompA = props => <h1>Primeiro Componente</h1>
const CompB = () => <h1>Segundo Componente</h1>

export default CompA
export { CompB }