import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

/* import CompA, { CompB } from './components/ExemploComponenteBasico'; */
import Membros from './components/Membros';

class App extends Component {
  constructor( props ) {
    super( props );
  }


  render() {
    return (
      <div className="App">
        <Membros nome="João" sobrenome="Silva" />
        <Membros nome="Maria" sobrenome="Silva" />
        <Membros nome="Pedro" sobrenome="Silva" />

        {/* <CompA />
        <CompB />
        <CompA />
        <CompA /> */}
      </div>
    
    );
  }
}


export default App;
