// exercicio 1

function calcularCirculo(objeto) {
    const pi = 3.14;
    const raio = objeto.raio;
    if(objeto.tipoCalculo === "A") {
        console.log(`A área do círculo é ${pi * raio ** 2}`);
    } else if(objeto.tipoCalculo === "C") {
        console.log(`A circunferência do círculo é ${2 * pi * raio}`);
    } 
}

const circulo1 = {
    raio: 4,
    tipoCalculo: "A"
}

const circulo2 = {
    raio: 4,
    tipoCalculo: "C"
}

calcularCirculo(circulo1);
calcularCirculo(circulo2);

// exercicio 2

function naoBissexto(ano) {
    if(ano % 400 === 0) {
        console.log(false);
    } else if (ano % 4 === 0 && ano % 100 !== 0) {
        console.log(false);
    } else if (ano % 4 !== 0) {
        if(ano % 400 !== 0) {
            console.log(true);
        } else {
            console.log(false);
        }
    }
}

const ano = 2000;

naoBissexto(ano);

let bissexto = ano => (ano % 400 === 0) || (ano % 4 === 0 && ano % 100 !== 0);
console.log(bissexto(2015));

// exercicio 3

function somarPares(array) {
    let soma = 0;
    for(let i = 0; i < array.length; i++) {
        if(i % 2 == 0) {
            soma = soma + array[i];
        }
    }
    return soma;
}

const arrayPares = [1, 56, 4.34, 6, -2];

console.log(somarPares(arrayPares));

// exercicio 4

function adicionar(valor1){
    return function (valor2){
        return valor1 + valor2;
    }
}
console.log(adicionar(5)(2));

// exercicio 5

function imprimirBRL(numero) {
    let num = numero.toString();
    let valor = parseFloat(num);
    return `R$${valor}`;
}

console.log(imprimirBRL(328.99));