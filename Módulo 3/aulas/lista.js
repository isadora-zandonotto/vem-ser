
function multiplicar(multiplicador, ...valores) {
    return valores.map(valor => valor * multiplicador);
}

 console.log(multiplicar(5, 3, 4));