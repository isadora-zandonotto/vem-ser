function criarSanduiche (pao, recheio, queijo, salada) {
    console.log(`Seu sanduíche tem o pão ${pao} 
    com recheio de ${recheio} 
    e queijo ${queijo}
    e salada de ${salada}`);
}

const ingredienes = ['3 queijos', 'Frango', 'Cheddar', 'Tomate e Alface']

//criarSanduiche(...ingredienes);

function receberValoresIndefinidos(...valores) {
    valores.map(valor => console.log(valor));
}

//receberValoresIndefinidos(1, 2, 3, 4, 5, 6);

console.log([..."Marcos"]);

let inputTest = document.getElementById('campoTeste');
inputTest.addEventListener('blur', () => {
    alert('Obrigado');
})

