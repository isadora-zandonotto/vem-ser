function cardapioIFood( veggie = true, comLactose = true ) {
    let cardapio = [
        'enroladinho de salsicha',
        'cuca de uva'
    ]
    
    if ( comLactose ) {
        cardapio.push( 'pastel de queijo' )
    }

    console.log({...cardapio});

     cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa']

    
    /* cardapio.concat( [
        'pastel de carne',
        'empada de legumes marabijosa'
    ] )  */

    if ( veggie ) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
       cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 )
       cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
    }

    console.log(cardapio);
    
    let resultado = cardapio
                        .filter(alimento => alimento.length > 12)
                        .map(alimento => alimento.toUpperCase());

   /* let resultadoFinal = []
    for (let i = 0; i < cardapio.length; i++) {
        resultadoFinal[i] = cardapio[i].toUpperCase()
        
    }*/

    return resultado;
}
    
    console.log(cardapioIFood(false));
//esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ] */
