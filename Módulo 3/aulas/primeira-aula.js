
var nomeDaVariavel = "valor";
let nomeDaLet = "ValorLet";
const nomeDaConst = "ValorConst";

var nomeDaVariavel = "Valor3";
nomeDaLet = "ValorLet2";

const nomeDaConst2 = {
    nome:"Marcos", 
    idade: 29
};

Object.freeze(nomeDaConst2);

nomeDaConst2.nome = "Marcos Henrique";


//console.log(nomeDaConst2);

//console.log(nomeDaLet);


function nomeDaFuncao(){
    //nomeDaVariavel = "valor";
    let nomeDaLet = "ValorLet2";

}

//console.log(nomeDaLet);

/*function somar(){

}

function somar(valor1, valor2 = 1){
    console.log(valor1 + valor2);
}

somar(3);
somar(3, 2);

console.log(1 + 1);
console.log(1 + "1"); */

function ondeMoro(cidade){
    //console.log("Eu moro em " + cidade + " e sou feliz.");
    console.log(`Eu moro em ${cidade} e sou feliz`);
}

//ondeMoro("Gravataí");

function fruteira(){
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                + "Goiaba"
                + "\n"
                + "Pêssego"
                + "\n"
    let newTexto = `
    Banana
        Ameixa
            Goiaba
                Pêssego
    `;

    console.log(texto);
    console.log(newTexto);

}

//fruteira();

let eu = {
    nome: "Marcos",
    idade: 29,
    altura: 1.85
};

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} anos e ${pessoa.altura} de altura`);
}

//quemSou(eu);

let funcaoSomarValores = function(a, b){
    return a + b;
}

let add = funcaoSomarValores
let resultado = add(3, 5)
//console.log(resultado);

const { nome:n, idade:i } = eu
//console.log(nome, idade);
//console.log(n, i);

const array = [1, 2, 3, 8];
const [n1, n3, n2, n4] = array;
//console.log(n1, n2, n3);

function testarPessoa({ nome, idade, altura }) {
    console.log(nome, idade, altura);
}
//testarPessoa(eu);

let a1 = 15;
let b1 = 10;
console.log(a1, b1);
[a1, b1] = [b1, a1];
console.log(a1, b1);

//a1 = a1 + b1; 
//b1 = a1 - b1;
//a1 = a1 - b1;
//console.log(b1);
//console.log(a1);



