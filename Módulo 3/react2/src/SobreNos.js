import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Card2 from './components/Card2';
import imgGato from './components/img/gatinho.jpg';
import Card3 from './components/Card3';
import './components/css/grid.css';


class SobreNos extends Component {
    render() {
        return(
            <div className="Home">
                <Header />
                <section className="container separacao background">
                    <div className="row">
                        <Card2 imagem={imgGato} class="img"></Card2>
                        <Card3/>
                    </div>
                    <div className="row">
                        <Card3/>
                        <Card2 imagem={imgGato} class="img"></Card2>
                    </div>
                    <div className="row">
                        <Card2 imagem={imgGato} class="img"></Card2>
                        <Card3/>
                    </div>
                    <div className="row">
                        <Card3/>
                        <Card2 imagem={imgGato} class="img"></Card2>
                    </div>
                </section>
                <Footer />
            </div>
        
        )
        
    }
}

export default SobreNos;