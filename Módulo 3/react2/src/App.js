import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Home from './Home';
import SobreNos from './SobreNos';
import Servicos from './Servicos';
import Contato from './Contato';

export default class App extends Component {

render() {
  return (
    <Router>
      <Route path="/" exact component={ Home }/>
      <Route path="/SobreNos" exact component={ SobreNos }/>
      <Route path="/Servicos" exact component={ Servicos }/>
      <Route path="/Contato" exact component={ Contato }/>
      {/* <Route path="/teste" component={ PaginaTeste } /> */}
    </Router>
  );
}
}

/* const PaginaTeste = () => 
  <div>
    Pagina Teste
    <Link to ="">Home</Link>
    <Link to ="/SobreNos">Sobre</Link>
    <Link to ="/Servicos">Servicos</Link>
    <Link to ="/Contato">Contato</Link>
  </div>  */