import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import ColFormulario from './components/ColFormulario';
import ColMap from './components/ColMap';

class Contato extends Component {
    render() {
        return(
            <div className="Home">
                <Header />
                <section class="container separacao background">
                <div class="row">
                    <ColFormulario/>
                    <ColMap/>
                </div>
                </section>
                <Footer/>
            </div>
        )
    }
}

export default Contato;