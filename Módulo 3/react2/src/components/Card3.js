import React, { Component } from 'react'
import Text from './TextBlock';
import './css/main-section.css';
import './css/reset.css';
import './css/grid.css';

export default class Card3 extends Component {
    render() {
        return (
            <div class="col col-12 col-md-9 col-lg-9">
                    <article>
                        <h1>Título</h1>
                        <Text />
                    </article>
                </div>
        )
    }
}