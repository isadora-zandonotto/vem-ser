import React, { Component } from 'react'
import './css/grid.css';
import './css/main-section.css';
import './css/reset.css';
import './css/grid.css';

export default class Card2 extends Component {
    render() {
        return (
            <div class="col col-12 col-md-3 col-lg-3">
                <div><img src={this.props.imagem} className={this.props.class} alt="gatinho" /></div>
            </div>
        )
    }
}