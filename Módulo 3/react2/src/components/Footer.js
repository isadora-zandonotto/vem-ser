import React, { Component } from 'react'
import UlMenu from './UlMenu'
import './css/footer.css'
import './css/grid.css'
import './css/reset.css'

export default class Footer extends Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="container">
                    <nav>
                        <UlMenu/>
                    </nav>
                    <p>
                        &copy Copyright DBC Comapny - 2019
                    </p>
                </div>
            </footer>
        )
    }
}
