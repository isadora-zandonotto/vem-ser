import React, { Component } from 'react'
import './css/main-section.css';

export default class ColMap extends Component {
    render() {
        return (
            <div class="col col-12 col-md-6 col-lg-6">
                <div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7200844760864!2d-51.170870285595235!3d-30.016192881892596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576528348634!5m2!1spt-BR!2sbr" width="600" height="800" frameborder="0" allowfullscreen="" class="map"></iframe>
                </div>
            </div>
        )
    }
}