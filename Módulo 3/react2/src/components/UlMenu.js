import React, { Component } from 'react'

export default class UlMenu extends Component {
    render() {
        return (
            <ul class="clearfix">
                <li>
                    <a href="index.html">Home</a>
                </li>
            
                <li>
                    <a href="sobrenos.html">Sobre nós</a>
                </li>
            
                <li>
                    <a href="servicos.html">Serviços</a>
                </li>
            
                <li>
                    <a href="contato.html">Contato</a>
                </li>
            </ul>
        )
    }
}