import React, { Component } from 'react'
import Button from './Button'
import Text from './TextBlock'

export default class ColFormulario extends Component {
    render() {
        return (
            <div class="col col-12 col-md-6 col-lg-6">
                <div>
                    <h1>Preencha com as suas informações de contato</h1>
                    <Text/>
                    <form>
                        <p>Nome:</p><input type="text" name="Nome" id="Nome" class="form"/>
                        <br/>
                        <p>Telefone:</p><input type="text" name="Telefone" id="Telefone" class="form"/>
                        <br/>
                        <p>Email:</p><input type="text" name="Email" id="Email" class="form"/>
                        <br/>
                        <p>Deixe sua mensagem:</p> <textarea name="Mensagem" id="Mensagem" cols="30" rows="10" class="form"></textarea>
                    </form>
                    <br/>
                    <Button class="button button-blue button-right" href="#" texto="Enviar"/>
                </div>
            </div>
        )
    }
}