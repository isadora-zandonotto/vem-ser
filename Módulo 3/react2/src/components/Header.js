import React, { Component } from 'react'
import logo from './img/logo-dbc-topo.png'
import UlMenu from './UlMenu'
import './css/header.css'
import './css/grid.css'
import './css/reset.css'
import './css/box.css'
import './css/main-section.css'


export default class Header extends Component {
    render() {
        return (
            <React.Fragment>
                <header className="main-header">
                    <nav className="container clearfix">
                    <a className="logo" href="index.html">
                        <img src={logo} alt="DBC Company"/>
                    </a>
                    
                    <label className="mobile-menu" for="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <input id="mobile-menu" type="checkbox"/>
                    <UlMenu/>
                </nav>
            </header>
            <div></div>
        </React.Fragment>
        
        )
    }
}