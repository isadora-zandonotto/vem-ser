import React, { Component } from 'react'
import Text from './TextBlock';
import Button from './Button';

export default class Card extends Component {
    render() {
        return (
            <div className="col col-12 col-md-6 col-lg-3">
                <article className="box">
                    <div><img src={this.props.imagem} alt="DBC 1" /></div>
                    <h4>Título</h4>
                    <Text />
                    <Button className="button button-blue" href="#" texto="Saiba mais" />
                </article>
            </div>
        )
    }
}