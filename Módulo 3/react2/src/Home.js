import React, { Component } from 'react';
import './App.css';
import './components/css/grid.css'
import './components/css/banner.css'
import Header from './components/Header';
import Footer from './components/Footer';
import Button from './components/Button';
import Text from './components/TextBlock';
import Card from './components/Card';
import imgColuna from './components/img/DBC1.png';
import imgColuna2 from './components/img/DBC2.png';
import imgColuna3 from './components/img/DBC3.png';
import imgColuna4 from './components/img/DBC4.png';

class Home extends Component {
    render() {
        return (
            <div className="Home">
                <Header />
                <section className="main-banner">
                    <article>
                        <h1>Vem ser DBC</h1>
                        <Text />
                        <Button class="button button-blue" href="#" texto="Saiba mais" />
                    </article>
                </section>
                <section className="container">
                    <div className="row">
                        <article className="col col-12 col-md-7 col-lg-7">
                            <h2>Título</h2>
                            <Text />
                        </article>
                        <article className="col col-12 col-md-5 col-lg-5">
                            <h3>Título</h3>
                            <Text />
                        </article>
                    </div>
                </section>
                <section className="container">
                    <div className="row">
                        <Card imagem={imgColuna}></Card>
                        <Card imagem={imgColuna2}></Card>
                        <Card imagem={imgColuna3}></Card>
                        <Card imagem={imgColuna4}></Card>
                    </div>
                </section>
                <Footer />
            </div>
        )
    }
}

export default Home;