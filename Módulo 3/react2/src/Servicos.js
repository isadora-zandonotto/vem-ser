import React, { Component } from 'react';
import Header from './components/Header';
import Card4 from './components/Card4';
import Footer from './components/Footer';
import Text from './components/TextBlock';
import Button from './components/Button';

class Servicos extends Component {
    render() {
        return(
            <div className="Home">
                <Header />
                <section class="container">
                <div class="row separacao">
                    <Card4/>
                    <Card4/>
                    <Card4/>
                    <Card4/>
                    <Card4/>
                    <Card4/>
                </div>
                </section>
                <section class="container separacao">
                <article class="box">
                    <h1>VEM SER DBC</h1>
                    <Text />
                    <Button class="button button-blue" href="#" texto="Inscreva-se"/>
                </article>
                </section>
                <Footer/>
            </div>
            
        )
    }
}

export default Servicos;