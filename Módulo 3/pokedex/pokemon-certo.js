class Pokemon { // eslint-disable-line no-unused-vars
  constructor(obj) {
    this.nome = obj.name;
    this.imagem = obj.sprites.front_default;
    this.id = obj.id;
    this.altura = obj.height * 10;
    this.peso = obj.weight / 10;
    this.tipos = `${obj.types.map(types => `<li>${types.type.name}<li>`).join('')}`
    this.estatisticas = `<strong>Stats:</strong><br> ${obj.stats.map(stats => `<li><strong>${stats.stat.name}</strong>: ${stats.base_stat} <li>`).join('')}`
  }
}