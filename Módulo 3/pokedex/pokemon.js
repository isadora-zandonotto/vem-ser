// QUESTÃO 01

// const pokemonIdInput = document.getElementById('pokemonIdInput');
// const pokemonsName = document.getElementById('pokemonsName');

// fetch('https://pokeapi.co/api/v2/pokemon/')
// .then(response => {
//     return response.json();
// })
// .then((jsonResponse) => {

//     const data = jsonResponse.results;

//     pokemonIdInput.addEventListener('blur', () => {
//         const pokemonId = parseInt(pokemonIdInput.value);
//         if(pokemonId > 0 && pokemonId <= 20) {
//             pokemonsName.innerHTML = `Nome: ${data[pokemonId-1]['name']}`;
//         } else {
//             alert('Digite uma quantidade válida');
//         }
//     });

// });

// QUESTÃO 02



const pokemonFetchURL = 'https://pokeapi.co/api/v2/pokemon/?offset=0&limit=803'
const allPokemonURL = 'https://pokeapi.co/api/v2/pokemon/?offset=0&limit=802';

const pokemonIdInputElement = document.getElementById('pokemonIdInput');
const pokemonsIdElement = document.getElementById('pokemonsId');
const pokemonsNameElement = document.getElementById('pokemonsName');
const pokemonsHeightElement = document.getElementById('pokemonsHeight');
const pokemonsWeigthElement = document.getElementById('pokemonsWeight');
const pokemonsImageElement = document.getElementById('pokemonsImage');
const pokemonTypeElement = document.getElementById('pokemonsType');
const pokemonStatsNameValueElement = document.getElementById('pokemonsStatsNameValue');
const pesquisar = document.getElementById('localizarId');
const pokemonsIndex = [];
let inputValue = 0;

fetch(pokemonFetchURL)
    .then(response => response.json())
    .then(jsonResponse => {
        initPokedex(jsonResponse);
        // console.log(jsonResponse);
    })

function initPokedex(jsonResponse) {
    const pokemonsData = jsonResponse.results;
   // let inputValue = 0;
    pesquisar.addEventListener('click', () => {
        const pokemonId = parseInt(pokemonIdInputElement.value);
        if (pokemonsIndex.includes(pokemonId)) {
            alert('Este pokemon já foi sorteado!')
        } else {
            if( pokemonId == inputValue ){
                alert("Id já está sendo exibido")
            }else{
                if (pokemonId > 0 && pokemonId <= 803) {
                    const pokemonUrl = pokemonsData[pokemonId - 1]['url'];
                    resolvePokemonUrl(pokemonUrl);
                    inputValue = pokemonId;
                } else {
                    alert('Digite uma quantidade válida');
                }
            }
        }
    });
}

function initPokedexRandom(jsonResponse) {
    const randomPokemonsData = jsonResponse.results;
    let randomPokemonIdIndex = getRandomIntInclusive(0, randomPokemonsData.length);
    let idPokemonRandom = randomPokemonIdIndex + 1;
    pokemonsIndex.push(idPokemonRandom);
    //console.log(randomPokemonIdIndex);
    const randomPokemonUrl = randomPokemonsData[randomPokemonIdIndex]['url'];
    //console.log(randomPokemonUrl);
    resolvePokemonUrl(randomPokemonUrl);
}

function resolvePokemonUrl(pokemonUrl) {
    fetch(pokemonUrl)
        .then(response => {
            return response.json();
        })
        .then(pokemonData => {console.log(pokemonData);
            pokemonsImageElement.src = pokemonData.sprites.front_default;
            pokemonsIdElement.innerHTML = `${pokemonData.id}`;
            pokemonsNameElement.innerHTML = `${pokemonData.name}`;
            pokemonsHeightElement.innerHTML = `${pokemonData.height * 10}cm`;
            pokemonsWeigthElement.innerHTML = `${pokemonData.weight / 10}kg`;
            pokemonTypeElement.innerHTML = `${pokemonData.types.map(types => `<li> ${types.type.name} </li>`).join("")} `;//pokemonData.types.map(types => types.type.name);
            pokemonStatsNameValueElement.innerHTML = `<strong>Stats:</strong><br> ${pokemonData.stats.map(stats => `<li><strong>${stats.stat.name}</strong>: ${stats.base_stat} <li>`).join("")}`;
            //console.log(pokemonData.types)
            //console.log(pokemonData);
        });
}

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const button = document.getElementById('estouComSorte');
button.addEventListener('click', () => {
    fetch(allPokemonURL)
        .then(response => response.json())
        .then(jsonResponse => {
            initPokedexRandom(jsonResponse);
            pokemonIdInputElement.value='';
            inputValue = 0;
        });
});


































