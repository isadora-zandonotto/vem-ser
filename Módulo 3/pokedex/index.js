const pokeApi = new PokeApi();
const pesquisar = document.querySelector('.pesquisar');
const sortear = document.querySelector('.sortear');
const pokemonIndex = [];
let inputValue = 0;
let inputIdPokemon = document.querySelector('.inputIdPokemon');

function renderizacaoPokemon(pokemon) {
  const dadosPokemon = document.getElementById('dadosPokemon');
  const nome = dadosPokemon.querySelector('.nome');
  const imgPokemon = dadosPokemon.querySelector('.thumb');
  const idPokemon = dadosPokemon.querySelector('.id');
  const alturaPokemon = dadosPokemon.querySelector('.altura');
  const pesoPokemon = dadosPokemon.querySelector('.peso');
  const tiposPokemon = dadosPokemon.querySelector('.tipos');
  const estatisticasPokemon = dadosPokemon.querySelector('.estatisticas');

  nome.innerHTML = pokemon.nome;
  idPokemon.innerHTML = pokemon.id;
  alturaPokemon.innerHTML = `${pokemon.altura}cm`;
  pesoPokemon.innerHTML = `${pokemon.peso}Kg`;
  tiposPokemon.innerHTML = `Tipos: ${pokemon.tipos}`
  estatisticasPokemon.innerHTML = pokemon.estatisticas;
  imgPokemon.src = pokemon.imagem;
}

async function buscar(id) {
  const pokemonEspecifico = await pokeApi.buscar(id);
  const poke = new Pokemon(pokemonEspecifico);
  if (id === inputValue) {
    alert('Id já está sendo exibido')
  } else if (id > 0 && id <= 803) {
    renderizacaoPokemon(poke);
    inputValue = id;
  } else {
    alert('Digite um id válido')
  }
}

function getRandomIntInclusive(min, max) {
  return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) + min;
}

async function buscarTodos() {
  let posicaoPokemonSorteado = getRandomIntInclusive(0, 4);
  const idPokemonSorteado = posicaoPokemonSorteado + 1;
  inputValue = idPokemonSorteado;
  //localStorage.setItem('numeros', '0');
  //quando busca o cache se o mesmo for nulo passar direto, caso contrario e o numero fazer parte sortear novamente
  /* if (localStorage.getItem('numero') == null || localStorage.getItem('numeros').length < 5) {
    const pokemonSorteado = await pokeApi.buscar(idPokemonSorteado);
    const poke = new Pokemon(pokemonSorteado);
    renderizacaoPokemon(poke);
    pokemonIndex.push(idPokemonSorteado);
    window.localStorage.setItem('numeros', JSON.stringify(pokemonIndex));
  } else if (localStorage.getItem('numeros').includes(inputValue)) {
    posicaoPokemonSorteado = getRandomIntInclusive(0, 4); 
  } else {
    alert('Todos os numeros já foram sorteados')
  } */
  if (localStorage.getItem('numero') != null && localStorage.getItem('numeros')) {
    posicaoPokemonSorteado = getRandomIntInclusive(0, 4);
  }/* 
  console.log(localStorage.getItem('numeros').includes(posicaoPokemonSorteado))*/
  console.log(localStorage.getItem('numeros')); 
  if (localStorage.getItem('numero') == null || localStorage.getItem('numeros').length < 5) {
    const pokemonSorteado = await pokeApi.buscar(idPokemonSorteado);
    const poke = new Pokemon(pokemonSorteado);
    renderizacaoPokemon(poke);
    pokemonIndex.push(idPokemonSorteado);
    window.localStorage.setItem('numeros', JSON.stringify(pokemonIndex));
  }

   /* if (localStorage.getItem('numeros').includes(inputValue)) {
    posicaoPokemonSorteado = getRandomIntInclusive(0, 4);
  } else if (localStorage.getItem('numeros').length <= 5) {
    const pokemonSorteado = await pokeApi.buscar(idPokemonSorteado);
    const poke = new Pokemon(pokemonSorteado);
    renderizacaoPokemon(poke);
    pokemonIndex.push(idPokemonSorteado);
    window.localStorage.setItem('numeros', JSON.stringify(pokemonIndex)); 
  } else {
    alert('Todos os numeros já foram sorteados')
  } */
  /* const pokemonSorteado = await pokeApi.buscar(idPokemonSorteado);
  const poke = new Pokemon(pokemonSorteado);
  renderizacaoPokemon(poke);
  pokemonIndex.push(idPokemonSorteado);
  window.localStorage.setItem('numeros', JSON.stringify(pokemonIndex)); */
}

pesquisar.addEventListener('click', () => {
  inputIdPokemon = document.querySelector('.inputIdPokemon');
  const idPokemonPesquisado = parseInt((inputIdPokemon.value), 10);
  if ( !isNaN(idPokemonPesquisado)) {
    buscar(idPokemonPesquisado);  
  } else {
    alert('Digite um valor válido')
  }
  
});

sortear.addEventListener('click', () => {
  buscarTodos();
  inputIdPokemon.value = '';
})