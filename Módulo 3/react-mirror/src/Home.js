import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';

export default class Home extends Component {
    render() {
        return (
            <Route>
                <Link to="/Mirror">Mirror</Link>
                <Link to="/JsFlix">JsFlix</Link>
            </Route>
        )
    }
}