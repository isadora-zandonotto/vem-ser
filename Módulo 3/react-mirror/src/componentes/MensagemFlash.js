import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MensagemFlash extends Component {

    constructor( props ) {
        super ( props )
        this.idsTimeouts = []
        this.animacao = ''
    }

    fechar = () => {
        this.props.atualizarMensagem( false )        
    }

    limparTimeOuts() {
        this.idsTimeouts.forEach( clearTimeout )
    }

    componentWillUnmount() {
        this.limparTimeOuts()
    }

    componentDidUpdate( prevPros ){
        const { deveExibirMensagem, tempo} = this.props
        if(prevPros.deveExibirMensagem !== deveExibirMensagem ) {
            const novoIdTimeout = setTimeout( () => {
                this.fechar()
            }, tempo * 3000 ) 
            this.idsTimeouts.push( novoIdTimeout )
        } 
    }

    render(){
        const { deveExibirMensagem, mensagem, cor, nota, tempo } = this.props
        /* if(deveExibirMensagem || nota){
            setTimeout( () => {
                this.props.atualizarMensagem(false)
              }, tempo )
        }
 */
        if( this.animacao || deveExibirMensagem ) {
            this.animacao = deveExibirMensagem ? 'fade-in' : 'fade-out'
        }
        return (
            <span onClick={ this.fechar } className={ `flash ${ cor !== 'verde' && cor !== 'vermelho' ? 'verde' : cor  } ${this.animacao } ${tempo} ` }>{ mensagem }</span>
        )
    }
}

// https://reactjs.org/docs/typechecking-with-proptypes.html
MensagemFlash.propTypes = {
    mensagem: PropTypes.string.isRequired,
    deveExibirMensagem: PropTypes.bool.isRequired,
    atualizarMensagem: PropTypes.func.isRequired,
    cor: PropTypes.oneOf(['verde','vermelho']),
    tempo: PropTypes.number
}

MensagemFlash.defaultProps = {
    cor: 'verde',
    tempo: 3
}