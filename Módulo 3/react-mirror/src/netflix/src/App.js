import React, { Component } from 'react';
import './App.css';
import ListaSeries from './ListaSeries';

export default class App extends Component {
  constructor( props ) {
    super (props);
    this.listaSeries = new ListaSeries()

    console.log( this.listaSeries.invalidas() );
    console.log( this.listaSeries.filtrarPorAno( 2020 ) );
    console.log( this.listaSeries.procurarPorNome( 'Winona Ryder' ) );
    console.log( this.listaSeries.mediaDeEpisodios() );
    console.log( this.listaSeries.totalSalarios( 2 ) );
    console.log( this.listaSeries.queroGenero( 'Suspense' ) );
    console.log( this.listaSeries.queroTitulo( 'The' ) );
    console.log( this.listaSeries.creditos(0));
  }
  render() {
    return (
      <div className="App">
        
      </div>
    );
  }
}

