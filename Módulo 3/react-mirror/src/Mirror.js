import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './componentes/EpisodioUi'
import MensagemFlash from './componentes/MensagemFlash';
import MeuInputNumero from './componentes/MeuInputNumero';
import { Link } from 'react-router-dom';



class Mirror extends Component {
  constructor(props) {
    super(props)
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem: false,
      nota: false,
      deveExibirErro: false,    
      mensagem: '',
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  assistido() {
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido( episodio )
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota( { nota, erro } ){
    this.setState({
      deveExibirErro: erro
    })
    if (erro){
      return;
    }

    let cor, mensagem;
    const { episodio } = this.state
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota )
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
    }else{
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida entre 1 e 5!'
    }
    
    this.exibirMensagem( { cor, mensagem } )
  }

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  /* avaliado(evt) {
    const { episodio } = this.state
    if(evt.target.value > 0 && evt.target.value < 6){
      episodio.avaliar(evt.target.value)
    this.setState({
      episodio,
      deveExibirMensagem: true,
      nota: false
    })
    } else {
      this.setState({
        nota: true
      })
    }
  } */
  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state
    const { listaEpisodios } = this

    return (
      <div className="App">
        { /*  deveExibirMensagem ? ( <span>Registramos sua nota!</span> ) : ''  */ }
         <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                        deveExibirMensagem={ exibirMensagem } 
                        mensagem={ mensagem }
                        cor={ cor }
                        segundos={ 5 } />
         <header className='App-header'>
          <EpisodioUi episodio={ episodio } />
          <div className="botoes">
            <button className="btn verde" onClick={ this.sortear.bind( this ) }>Próximo</button>
            <button className="btn azul" onClick={ this.assistido.bind( this ) }>Já Assisti</button>
            <button className="btn vermelho">
              <Link to= { {pathname: "/avaliacoes", state: {listaEpisodios} }}>Avaliações</Link ></button>
          </div>
          { /* this.geraCampoDeNota() */ }
          <MeuInputNumero placeholder="1 a 5"
                          mensagemCampo="Qual sua nota para esse episódio?"
                          visivel={ episodio.assistido || false }
                          obrigatorio={ true }
                          atualizarValor={ this.registrarNota.bind( this ) }
                          deveExibirErro={ deveExibirErro }
          />
        </header>
      </div>
    );
  }
}

export default Mirror;