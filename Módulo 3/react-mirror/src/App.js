import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Mirror from './Mirror';
import Home from './Home';
import JsFlix from './models/Info'
import ListaAvaliacoes from './componentes/ListaAvaliacoes';
import TelaDetalheEpisodio from './componentes/TelaDetalheEpisodio';


export default class App extends Component {

render() {
  return (
    <Router>
      <Route path="/" exact component={ Home }/>
      <Route path="/Mirror" component={ Mirror } />
      <Route path="/JsFlix" component={ JsFlix }/>
      <Route path="/avaliacoes" component={ ListaAvaliacoes }/>
      <Route path="/episodio/:id" component={ TelaDetalheEpisodio }/>

    </Router>
  );
}
}

/* const PaginaTeste = () => 
  <div>
    Pagina Teste
    <Link to ="/">Home</Link>
    <Link to ="/Mirror">React Mirror</Link>
    <Link to ="/JsFlix">InfoJsFlix</Link>
  </div>
 */