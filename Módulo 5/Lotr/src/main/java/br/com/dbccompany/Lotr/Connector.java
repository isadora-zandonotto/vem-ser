package br.com.dbccompany.Lotr;

import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.util.logging.Level;

/* Arquivo de conexão com o banco de dados oracle, verifica se a conexão é válida.
 * Caso não possua conexão cria uma classe com o drive de conexão, seta os dados
 * e executa um get
 * 
 * ERRO: caso caia no catch criar um log de erro de conexão*/


public class Connector {
	private static Connection conn;
	
	public static Connection connect() {
		try {
			if(conn!=null && conn.isValid(10)) {
				return conn;
			}
			
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:49161:XE", "system", "oracle");  
		
		} catch(ClassNotFoundException | SQLException ex) {
			Logger.getLogger(Connector.class.getName()).log(Level.SEVERE,"erro de conexão", ex);
		}
		return conn;
	}
}
