package br.com.dbccompany.Lotr;

import org.hibernate.Session;
import org.hibernate.Transaction;
import br.com.dbccompany.Lotr.Entity.HibernateUtil;
import br.com.dbccompany.Lotr.Entity.PersonagemEntity;


public class Main {
	
	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			PersonagemEntity personagem = new PersonagemEntity();
			personagem.setNome("Qualquer");
			
			session.save(personagem);
			
			transaction.commit();
		}catch(Exception e){
			if(transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	}
	
	


//	public static void main(String[] args) {
//		Connection conn = Connector.connect();
//		
//		try{
//			
//			ResultSet rs = conn.prepareStatement("select tname from tab where tname='PAÍSES'")
//					.executeQuery();
//			if(!rs.next()) {
//				conn.prepareStatement("CREATE TABLE PAÍSES(\n"
//						+ "ID_PAIS INTEGER PRIMARY KEY NOT NULL, \n"
//						+ "NOME VARCHAR(100) NOT NULL\n"
//						+")").execute();
//			}
//			
//			PreparedStatement pst = conn.prepareStatement("INSERT INTO PAÍSES(ID_PAIS, NOME)"
//					+ "VALUES(SEQ_PAISES.NEXTVAL, ?)");
//			pst.setString(1, "Brasil");
//			pst.executeUpdate();
//			
//			rs = conn.prepareStatement("SELECT * FROM PAÍSES").executeQuery();
//			while(rs.next()) {
//				System.out.println(String.format("Nome do país:%s", rs.getString("NOME")));
//			}
//			
//		}catch(SQLException e) {
//			Logger.getLogger(Connector.class.getName()).log(Level.SEVERE,"ERRO NA CONSULTA", e);
//		}
//
//	}
//
}
