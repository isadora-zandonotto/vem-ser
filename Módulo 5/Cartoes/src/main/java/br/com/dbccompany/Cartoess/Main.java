package br.com.dbccompany.Cartoess;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoess.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoess.Entity.CartaoEntity;
import br.com.dbccompany.Cartoess.Entity.ClienteEntity;
import br.com.dbccompany.Cartoess.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoess.Entity.EmissorEntity;
import br.com.dbccompany.Cartoess.Entity.HibernateUtil;
import br.com.dbccompany.Cartoess.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoess.Entity.LojaEntity;
import br.com.dbccompany.Cartoess.Entity.LojaXCredenciador;


public class Main {
	
	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja = new LojaEntity();
			loja.setNome("Apple");
			
			List<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Fulano");
			
			CredenciadorEntity credenciador = new CredenciadorEntity();
			credenciador.setNome("Isadora");
			
			List<CredenciadorEntity> credenciadores = new ArrayList<>();
			credenciadores.add(credenciador);
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("Visa");
			bandeira.setTaxa(0.1);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Aleatorio");
			emissor.setTaxa(0.1);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setBandeira(bandeira);
			cartao.setChip(1);
			cartao.setCliente(cliente);
			cartao.setEmissor(emissor);
			cartao.setVencimento("12/21");
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setCartao(cartao);
			lancamento.setData_compra("10/02/2020");
			lancamento.setDescricao("débito");
			lancamento.setValor(100.0);
			
			LojaXCredenciador lojaXCreden = new LojaXCredenciador();
			lojaXCreden.setCredenciador(credenciadores);
			lojaXCreden.setLoja(lojas);
			lojaXCreden.setTaxa(0.8);
			
			session.save(loja);
			session.save(credenciador);
			session.save(bandeira);
			session.save(emissor);
			session.save(cliente);
			session.save(cartao);
			session.save(lancamento);
			session.save(lojaXCreden);
			
			System.out.println("Valor para Credenciador: " + lancamento.getValor() * lojaXCreden.getTaxa());
			System.out.println("Valor para Bandeira: " + lancamento.getValor() * bandeira.getTaxa());
			System.out.println("Valor para Emissor: " + lancamento.getValor() * emissor.getTaxa());
			
			transaction.commit();
		}catch(Exception e){
			if(transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	
	}
}
	