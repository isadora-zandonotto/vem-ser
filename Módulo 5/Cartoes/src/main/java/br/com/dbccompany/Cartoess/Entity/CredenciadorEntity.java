package br.com.dbccompany.Cartoess.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import br.com.dbccompany.Cartoess.Entity.LojaXCredenciador;

@Entity
@Table(name="CREDENCIADOR")
@SequenceGenerator(allocationSize = 1, name = "CREDENCIADOR_SEQ", sequenceName = "CREDENCIADOR_SEQ")
public class CredenciadorEntity {

	@Id
	@GeneratedValue(generator = "CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_CREDENCIADOR", nullable=false)
	private Integer id;
	private String nome;
	
	@ManyToMany(mappedBy = "credenciador")
	private List<LojaXCredenciador> lojaXCredenciador = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<LojaXCredenciador> getLojaXCredenciador() {
		return lojaXCredenciador;
	}
	public void setLojaXCredenciador(List<LojaXCredenciador> lojaXCredenciador) {
		this.lojaXCredenciador = lojaXCredenciador;
	}
	
	
}
