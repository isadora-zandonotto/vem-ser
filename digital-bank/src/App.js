import React from 'react';
import SignIn from './pages/SignIn';
import axios from 'axios';
import Home from './pages/Home'
import Agencias from './pages/Agencias'

// import Contas from './pages/contas'
// import TiposDeContas from './pages/tiposDeContas'
// import ContaDeClientes from './pages/contaDeClientes'

import { BrowserRouter as Router, Route } from 'react-router-dom';

let TOKEN = "banco-vemser-api-fake";

const LOGIN_URL = 'http://localhost:1337/login';

// Credentials:
// vemser@bancodigital.com
// vemserbanco;

async function login({ email, password }) {
    await axios.post(LOGIN_URL, { 
        email: email,
        senha: password 
    }).then(response => {
        TOKEN = response.data.token;
        console.log(response.data.token);
        
    }).catch(err => {
        console.log('Invalid Credentials');
    });
}

function isTokenDefined() {
    return !(TOKEN === null || TOKEN === "");
}

function App() {
    return (
        <Router>
            <Route exact path='/home' component = { () => isTokenDefined() ? <Home/> : <SignIn onSubmit={login} /> } />
            <Route path='/agencias' component = { () => isTokenDefined() ? <Agencias/> : <SignIn onSubmit={login} /> } />
            {/* <Route path='/contas' component = { () => isTokenDefined() ? <Contas/> : <SignIn onSubmit={login} /> } />
            <Route path='/tipos-de-contas' component = { () => isTokenDefined() ? <TiposDeContas/> : <SignIn onSubmit={login} /> } />
            <Route path='/conta-de-clientes' component = { () => isTokenDefined() ? <ContaDeClientes/> : <SignIn onSubmit={login} /> } /> */}
        </Router>
    );
}

export default App;