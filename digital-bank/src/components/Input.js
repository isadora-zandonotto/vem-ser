import React, { Component } from 'react'

class Input extends Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.props.onChange(event.target);
    }

    render() {
        return (
            <React.Fragment> 
                <input
                    name={this.props.name}
                    type={this.props.type} 
                    placeholder={this.props.placeholder} 
                    onChange={this.handleChange} />
            </React.Fragment>
        )
    }
}

export default Input;