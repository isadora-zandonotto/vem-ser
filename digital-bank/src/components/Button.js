import React, { Component } from 'react'
import { Link } from 'react-router-dom';


class Button extends Component {

    constructor(props) {
        super(props);
        this.handleCLick = this.handleCLick.bind(this);
    }

    handleCLick(event) {
        event.preventDefault();
        this.props.onClick(event.target);
    }

    render() {
        return (
            <React.Fragment> 
                <Link to="/Home"><button onClick={this.handleCLick}>{this.props.text}</button></Link>
            </React.Fragment>
        )
    }
}

export default Button;