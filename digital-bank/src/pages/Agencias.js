import React, { Component } from 'react';
import axios from 'axios';

export default class Agencias extends Component{

    getAgencias(){
        axios.get('http://localhost:1337/agencias', { headers: { 'Authorization': 'banco-vemser-api-fake' } })
        .then((response) => {
            console.log(response.data);
        }).catch((err) => {
            console.log(err);
        })
    }

    

    render(){
        this.getAgencias()
        return(
            <h1>Agencias</h1>
        )
    }
}