import React, { Component } from 'react'
import Input from '../components/Input'
import Button from '../components/Button'

class SignIn extends Component {

    constructor(props) {
        super(props);
        this.state = { email: "", password: "" }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const name = event.name;
        const value = event.value;
        this.setState({
            [name]: value
        });
    }

    handleSubmit() {
        this.props.onSubmit(this.state);
    }

    render() {
        return (
            <React.Fragment>
                <div className="login">
                    <h2 className="login-header">Log in</h2>
                    <form> 
                        <Input 
                            onChange={this.handleInputChange} 
                            name="email" 
                            label="Email" 
                            type="text" 
                            placeholder="email" />
                        <Input 
                            onChange={this.handleInputChange} 
                            name="password" 
                            label="Password" 
                            type="password" 
                            placeholder="password" />
                        <Button 
                            onClick={this.handleSubmit}
                            text="Submit" />
                    </form>
                </div >
            </React.Fragment>
        )
    }
}

export default SignIn;