import React from 'react';
import { Link } from 'react-router-dom';


function Home() {
    return(
        <div>
            <section>
                <h1>Digital Bank - Home</h1>
            </section>
            <section>
                <Link to="/Agencias"><p><a>Agencias</a></p></Link>
                <Link to="/Clientes"><p><a>Clientes</a></p></Link>
                <Link to="/TiposDeContas"><p><a>Tipos de contas</a></p></Link>
                <Link to="/ContaDeClientes"><p><a>Contas de clientes</a></p></Link>
            </section>
        </div>
    );
}

export default Home;