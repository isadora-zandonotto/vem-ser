package br.com.dbccompany.bancodigital.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.dbccompany.bancodigital.Entity.Banco;

import java.util.List;

@Repository
public interface BancoRepository extends CrudRepository<Banco, Integer> {

    Banco findByNome(String nome);
    List<Banco> findAll();
}
