package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table(name="MOVIMENTACAO")
@SequenceGenerator(allocationSize = 1, name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ")

public class Movimentacao {
    @Id
    @GeneratedValue(generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "MOVIMENTACAO_SEQ", nullable=false)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private Tipo tipo;
    private Integer valor;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CONTA")
    private Contas conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Contas getConta() {
        return conta;
    }

    public void setConta(Contas conta) {
        this.conta = conta;
    }

}
