package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table(name="CONTAS")
@SequenceGenerator(allocationSize = 1, name = "CONTAS_SEQ", sequenceName = "CONTAS_SEQ")

public class Contas {
    @Id
    @GeneratedValue(generator = "CONTAS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTA", nullable=false)
    private Integer id;
    private Integer numero;
    private Integer saldo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_AGENCIA")
    private Agencias agencia;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_TIPO")
    private Tipos tipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getSaldo() {
        return saldo;
    }

    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }

    public Agencias getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencias agencia) {
        this.agencia = agencia;
    }

    public Tipos getTipo() {
        return tipo;
    }

    public void setTipo(Tipos tipo) {
        this.tipo = tipo;
    }
}
