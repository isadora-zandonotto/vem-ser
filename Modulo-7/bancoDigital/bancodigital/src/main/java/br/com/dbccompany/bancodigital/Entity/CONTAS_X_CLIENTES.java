package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="CONTAS_X_CLIENTES")
@SequenceGenerator(allocationSize = 1, name = "CONTAS_X_CLIENTES_SEQ", sequenceName = "CONTAS_X_CLIENTES_SEQ")
public class CONTAS_X_CLIENTES {
    @Id
    @GeneratedValue(generator="CONTAS_X_CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "CONTAS_X_CLIENTES_SEQ")
    private Integer id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name="id_contas",
            joinColumns = {
                    @JoinColumn(name = "id_contas_x_clientes")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_contas")})
    private List<Contas> conta = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name="id_clientes",
            joinColumns = {
                    @JoinColumn(name = "id_contas_x_clientes")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_clientes")})
    private List<Clientes> cliente = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Contas> getConta() {
        return conta;
    }

    public void setConta(List<Contas> conta) {
        this.conta = conta;
    }

    public List<Clientes> getCliente() {
        return cliente;
    }

    public void setCliente(List<Clientes> cliente) {
        this.cliente = cliente;
    }
}
