package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
@Entity
@Table(name="TIPOS")
@SequenceGenerator(allocationSize = 1, name = "TIPOS_SEQ", sequenceName = "TIPOS_SEQ")
public class Tipos {
    @Id
    @GeneratedValue(generator = "TIPOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "TIPOS_SEQ", nullable=false)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private Descricao descricao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Descricao getDescricao() {
        return descricao;
    }

    public void setDescricao(Descricao descricao) {
        this.descricao = descricao;
    }
}
