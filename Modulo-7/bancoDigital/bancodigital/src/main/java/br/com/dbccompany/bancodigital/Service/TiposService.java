package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Tipos;
import br.com.dbccompany.bancodigital.Repository.TiposRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TiposService {
    @Autowired
    private TiposRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Tipos salvar(Tipos tipo) {
        return repository.save(tipo);
    }

    @Transactional(rollbackFor = Exception.class)
    public Tipos editar(Tipos tipo, Integer id) {
        tipo.setId(id);
        return repository.save(tipo);
    }

    public List<Tipos> todosTipos() {
        return(List<Tipos>) repository.findAll();
    }

    public Tipos tipoEspecifico(Integer id) {
        Optional<Tipos> tipo = repository.findById(id);
        return tipo.get();
    }
}
