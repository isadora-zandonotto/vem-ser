package br.com.dbccompany.bancodigital.Entity;
import javax.persistence.*;

@Entity
@Table(name="CIDADES")
@SequenceGenerator(allocationSize = 1, name = "CIDADES_SEQ", sequenceName = "CIDADES_SEQ")
public class Cidades {
    @Id
    @GeneratedValue(generator = "CIDADES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "CIDADES_SEQ", nullable=false)
    private Integer id;
    private String nome;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_ESTADO")
    private Estados estado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estados getEstado() {
        return estado;
    }

    public void setEstado(Estados estado) {
        this.estado = estado;
    }
}
