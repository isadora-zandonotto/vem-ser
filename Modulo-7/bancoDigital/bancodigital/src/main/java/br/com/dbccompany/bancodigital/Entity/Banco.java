package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table(name="BANCO")
public class Banco {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
    @GeneratedValue(generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_BANCO", nullable=false)
    private Integer id;
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
