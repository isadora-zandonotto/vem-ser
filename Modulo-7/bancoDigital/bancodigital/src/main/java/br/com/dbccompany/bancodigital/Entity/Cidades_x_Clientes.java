package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="CIDADES_X_CLIENTES")
@SequenceGenerator(allocationSize = 1, name = "CIDADES_X_CLIENTES_SEQ", sequenceName = "CIDADES_X_CLIENTES_SEQ")
public class Cidades_x_Clientes {
    @Id
    @GeneratedValue(generator="CIDADES_X_CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "CIDADES_X_CLIENTES_SEQ")
    private Integer id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name="id_clientes",
            joinColumns = {
                    @JoinColumn(name = "id_cidades_x_clientes")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_clientes")})
    private List<Clientes> cliente = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name="id_cidades",
            joinColumns = {
                    @JoinColumn(name = "id_cidades_x_clientes")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_cidades")})
    private List<Cidades> cidade = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Clientes> getCliente() {
        return cliente;
    }

    public void setCliente(List<Clientes> cliente) {
        this.cliente = cliente;
    }

    public List<Cidades> getCidade() {
        return cidade;
    }

    public void setCidade(List<Cidades> cidade) {
        this.cidade = cidade;
    }
}
