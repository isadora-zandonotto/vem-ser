package br.com.dbccompany.bancodigital.Repository;



import br.com.dbccompany.bancodigital.Entity.Descricao;
import br.com.dbccompany.bancodigital.Entity.Tipos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TiposRepository extends CrudRepository<Tipos, Integer> {

    Tipos findByDescricao(Descricao descricao);
    List<Tipos> findAll();
}
