package br.com.dbccompany.bancodigital.Controller;


import br.com.dbccompany.bancodigital.Entity.Tipos;
import br.com.dbccompany.bancodigital.Service.TiposService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipos")
public class TiposController {

    @Autowired
    TiposService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Tipos> todosTipos(){
        return  service.todosTipos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Tipos novoTipo(@RequestBody Tipos tipo){
        return service.salvar(tipo);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Tipos editarTipo(@PathVariable Integer id, @RequestBody Tipos tipo){
        return service.editar(tipo, id);
    }
}
