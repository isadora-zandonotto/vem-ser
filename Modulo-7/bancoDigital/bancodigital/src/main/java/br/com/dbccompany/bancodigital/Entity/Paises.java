package br.com.dbccompany.bancodigital.Entity;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="PAISES")
@SequenceGenerator(allocationSize = 1, name = "PAISES_SEQ", sequenceName = "PAISES_SEQ")
public class Paises {
    @Id
    @GeneratedValue(generator = "PAISES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "PAISES_SEQ", nullable=false)
    private Integer id;
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
