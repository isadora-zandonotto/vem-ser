import java.util.*;
public class AgendaContatos {
    private HashMap<String, String> agenda;
    private String nome, telefone;
    
    public AgendaContatos(){
        agenda = new LinkedHashMap<>();
    }
    
    public void adicionarContato( String nome, String telefone ){
        agenda.put(nome, telefone);
    }
    
    public String obterTelefone(String nome){
        return agenda.get(nome);
    }
    
    
    public String consultarPorTelefone( String telefone ){
        for(HashMap.Entry<String, String> par : agenda.entrySet() ){
            if( par.getValue().equals(telefone)){
                return par.getKey();
            }
        }
        return null;
    }
    
    public String cdv(){
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for( HashMap.Entry<String, String> par : agenda.entrySet()){
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s, %s%s", chave, valor, separador);
            builder.append(contato);
        
        }
        return builder.toString(); 
    }
   
}
