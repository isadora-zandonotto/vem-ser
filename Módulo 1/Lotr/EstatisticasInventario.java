
public class EstatisticasInventario {
    
    private Inventario inventario;

    public EstatisticasInventario (Inventario inventario){
        this.inventario = inventario;
    }

    public double calcularMediaItens(){
        ///Calcular Media:
        //Pegar o total de itens no inventario, percorrer linha a linha somando a quantidade de cada item
        //e no final dividir pelo total de itens
        if (this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        double somaQtdTotal = 0;
        for(Item item : this.inventario.getItens()){
            somaQtdTotal = somaQtdTotal + item.getQuantidade();
        }

        return somaQtdTotal / this.inventario.getItens().size();
    }

    /*public double medianaQtdItens() {
        if(inventario.getItens().size() % 2 != 0) {
            int posicao = (inventario.getItens().size() / 2);
            return inventario.obterItem(posicao).getQuantidade();
        } else {
            int qtdPosicao1 = inventario.obterItem(inventario.getItens().size() / 2).getQuantidade();
            int qtdPosicao2 = inventario.obterItem((inventario.getItens().size() / 2 ) - 1).getQuantidade();
            return (qtdPosicao1 + qtdPosicao2) / 2;
        }
    } */
    
    public double calcularMediana() {
     if(this.inventario.getItens().isEmpty()){
        return Double.NaN; 
        }
        
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obterItem(meio).getQuantidade();
        if(qtdItens % 2 == 1) {
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obterItem(meio - 1).getQuantidade();
        return ( qtdMeio + qtdMeioMenosUm ) / 2.0;
    
    }
  

    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMediaItens();
        int itensAcimaDaMedia = 0;
        
        for(Item item : this.inventario.getItens()) {
            if(item.getQuantidade() > media) {
                itensAcimaDaMedia++;
            }
        } 
        
        return itensAcimaDaMedia;
    }
}

