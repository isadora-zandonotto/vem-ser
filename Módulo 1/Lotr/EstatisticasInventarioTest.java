

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {
    
    @Test
    public void calcularMediaInventarioVazio(){
         Inventario inventario = new Inventario();
         EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
         assertTrue(Double.isNaN(estatisticas.calcularMediaItens()));
    }
    
    @Test
    public void calcularMediaApenasUmItem(){
         Inventario inventario = new Inventario();
         inventario.adicionarItem(new Item(2, "Escudo"));
         EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
         assertEquals(2, estatisticas.calcularMediaItens(), 1e-9);
    }
    
    @Test
    public void calcularMediaApenasDoisItens(){
         Inventario inventario = new Inventario();
         inventario.adicionarItem(new Item(2, "Escudo"));
         inventario.adicionarItem(new Item(4, "Espada"));
         EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
         assertEquals(3, estatisticas.calcularMediaItens(), 1e-9);
    }
    
    @Test
    
    public void calcularMedianaImpar(){
         Inventario inventario = new Inventario();
         inventario.adicionarItem(new Item(2, "Escudo"));
         inventario.adicionarItem(new Item(4, "Espada"));
         inventario.adicionarItem(new Item(1, "Arco"));
         EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
         assertEquals(4, estatisticas.calcularMediana(), 1e-9);
    }
     @Test
    
    public void calcularMedianaPar(){
         Inventario inventario = new Inventario();
         inventario.adicionarItem(new Item(2, "Escudo"));
         inventario.adicionarItem(new Item(4, "Espada"));
         inventario.adicionarItem(new Item(1, "Arco"));
         inventario.adicionarItem(new Item(3, "Flecha"));
         EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
         assertEquals(2.5, estatisticas.calcularMediana(), 1e-9);
    }
}
