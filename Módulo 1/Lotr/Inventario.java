import java.util.*;

public class Inventario {
     private ArrayList<Item> itens = new ArrayList<>();
    
    public Inventario() {
            this.itens = new ArrayList<Item>();
    }
    
    public ArrayList<Item> getItens(){
            return this.itens;
    }
    
    public Item buscar(String descricao) {
        for(Item item : this.itens) {
            if(item.getDescricao().equals(descricao)) {
                return item;
            }
        }
        return null;
    }
    
    public void adicionarItem(Item item) {
        this.itens.add(item);
    }
    
    public Item obterItem(int posicao) {
        if( posicao >= this.itens.size()) {
            return null;
        }
        return this.itens.get(posicao);
    }
    
    public void removerItem(Item item) {
        this.itens.remove(item);
    }
    
    public String getDescricoesItens() {
        StringBuilder resultado = new StringBuilder();
        int i = 0;
        for(Item item : itens) {
            if (i == 0) {
                resultado.append(item.getDescricao());
                i++;
            } else {
                resultado.append(", " + item.getDescricao());
            }
        }
        return resultado.toString();
    }
    
    public int getQuantidadeItensTotal(){
        int quantidadeItens = 0;
        for(Item item : itens){
            item.getQuantidade();
            quantidadeItens++;
        } 
    
        return quantidadeItens;
    }
    
    
    
    //    public String getDescricoesItensAula() {
    //        StringBuilder descricoes = new StringBuilder();
    //
    //        for( int i = 0; i < this.itens.size(); i++) {
    //            Item item = this.itens.get(i);
    //
    //            if( item != null){
    //                descricoes.append(item.getDescricao());
    //                descricoes.append(",");
    //            }
    //        }
    //
    //        return(descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() -1)) : descricoes.toString());
    //
    //    }
    
    
    
        public Item retornarItemComMaiorQuantidade() {
    
        Item itemComMaiorQuantidade = null;
    
        for(Item item : this.itens) {
            if(itemComMaiorQuantidade == null || item.getQuantidade() > itemComMaiorQuantidade.getQuantidade()) {
                itemComMaiorQuantidade = item;
            }
        }
        return itemComMaiorQuantidade;
    }
    
    /*
     public Item getItemComMaiorQuantidadeAula() {
        int indice = 0; maiorQuantidade = 0;
    
            for( int i = 0; i < this.itens.length; i++){
                 Item item = this.itens[i];
                 if ( item.getQuantidade() > maiorQuantidade) {
                     maiorQuantidade = item.getQuantidade();
                     indice = i;
                 }
            }
    
            return this.itens.length > 0 ? this
        }
     
    
    public Inventario Inverter() {
        Inventario itensInvertidos = new Inventario();
        for(int i = this.itens.size() -1; i >= 0; i--) { // caso não possa usar o .size criar um método que itere sobre a lista e diga o tamanho do array.
           itensInvertidos.adicionarItem(this.itens.get(i));
        }
        return itensInvertidos;
    }*/
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> itensInvertidos = new ArrayList<>(this.itens.size());
        for( int i = this.itens.size() - 1; i >= 0; i--) {
            itensInvertidos.add(this.itens.get(i));
        }
        return itensInvertidos;
     }
     
     public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
     public void ordenarItens(TipoOrdenacao ordenacao){
         for(int i = 0; i < this.itens.size(); i++){
             for (int j = 0; j < this.itens.size() - 1; j++){
                 Item atual = this.itens.get(j);
                 Item proximo = this.itens.get(j + 1);
                 boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
                 atual.getQuantidade() > proximo.getQuantidade():
                 atual.getQuantidade() < proximo.getQuantidade();
                 
                 if(deveTrocar){
                     Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, itemTrocado);
                 }
             }
         }
        }
        
        
    } 
        
    
    
     
    
    // itensInvertidos = []
    // itens = [1, 5, 3, 4];
    // i = 3;
    
    // itensInvertidos = [4]
    // itens = [1, 5, 3, 4];
    // i = 2;
    
    // itensInvertidos = [4, 3]
    // itens = [1, 5, 3, 4];
    // i = 1;
    
    // itensInvertidos = [4, 3, 5]
    // itens = [1, 5, 3, 4];
    // i = 0;
    
    // itensInvertidos = [4, 3, 5, 1]
    // itens = [1, 5, 3, 4];
    // i = -1;
    



