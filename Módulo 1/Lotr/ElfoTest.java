

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    
    public void atirarFlechaDiminuirFlechaAumentarXp() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Y");
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQuantidadeFlecha());
        
    }
    
    @Test
    
    public void atirar2FlechasDiminuirFlechasAumentarXp() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Y");
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQuantidadeFlecha());
        
    }
    
    @Test
    
    public void atirar3FlechasDiminuirFlechasAumentarXp() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Y");
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQuantidadeFlecha());
        
    } 
   
    @Test
    
    public void atirarFlechaEmDwarfDiminuirVidaDwarfAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        assertEquals(100.0, novoDwarf.getQuantidadeDeVida(), 0.001);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQuantidadeFlecha());
        
        
        
        
    } 
    
    @Test
    
    public void atirar2FlechaEmDwarfDiminuirVidaDwarfDiminuir2Flechas(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        novoElfo.atirarFlechaNoDwarf(novoDwarf);
        assertEquals(90.0, novoDwarf.getQuantidadeDeVida(), 0.001);
        assertEquals(0, novoElfo.getQuantidadeFlecha());
        
        
        
        
    }
    
    @Test
    
    public void elfoNasceComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }

    @Test
    public void naoCriarElfoNaoIncrementa(){
        assertEquals(0, Elfo.getQtdElfos());
    }


    
   
    
    
}
