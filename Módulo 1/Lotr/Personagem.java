
public class Personagem {
    
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double quantidadeDeVida, qtdDano, vidaGanha;
    protected int experiencia;
    protected int qtdExperienciaPorAtaque;
  
    
    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario();
        experiencia = 0;
        qtdExperienciaPorAtaque = 1;
        qtdDano = 0;
        vidaGanha = 0;
    }
    
    public Personagem(String nome){
        this.nome = nome;
    }
    
    public String  getNome() {
        return this.nome;
    }

    public void setNome ( String nome ) {
        this.nome = nome;
    }

    public Status getStatus() {
        return this.status;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }

    public double getQuantidadeDeVida() {
        return this.quantidadeDeVida;

    }
    
    public boolean podeSofrerDano() {
        return this.quantidadeDeVida > 0;
    }
    
    public void ganharItem(Item item){
        this.inventario.adicionarItem(item);
    }
    
    public void perderItem(Item item){
        this.inventario.removerItem(item);
    }
    
    public void aumentarXp() {
        experiencia = experiencia + qtdExperienciaPorAtaque;
    }
    
    public void sofrerDano() {
        if( this.podeSofrerDano() && qtdDano > 0.0 ) {
            this.quantidadeDeVida = this.quantidadeDeVida >= this.qtdDano ?
            this.quantidadeDeVida - this.qtdDano : 0.0;
            
            if(this.quantidadeDeVida == 0.0){
                this.status = Status.MORTO;
            } else {
                this.status = Status.SOFREU_DANO;
            }
        }
    }
    
    public void ganharVida(){
        if(this.podeSofrerDano() && this.vidaGanha > 0.0 ) {
            this.quantidadeDeVida = this.quantidadeDeVida + this.vidaGanha;
        }
    }
   
}
