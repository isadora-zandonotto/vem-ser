

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
    
    @Test
    public void elfoNoturnoQtdVida85PerdeFlecha(){
    ElfoNoturno novoElfoNoturno = new ElfoNoturno("X");
    Dwarf novoDwarf = new Dwarf("Y");
    novoElfoNoturno.atirarFlechaNoDwarf(novoDwarf);
    assertEquals(85.0, novoElfoNoturno.getQuantidadeDeVida(), 1e-9);
    assertEquals(1, novoElfoNoturno.getFlecha().getQuantidade());
    }
    
    @Test
    public void atiraFlechaEGanha3Xp(){
    ElfoNoturno novoElfoNoturno = new ElfoNoturno("X");
    Dwarf novoDwarf = new Dwarf("Y");
    novoElfoNoturno.atirarFlechaNoDwarf(novoDwarf);
    assertEquals(3, novoElfoNoturno.getExperiencia());
    }

    
}
