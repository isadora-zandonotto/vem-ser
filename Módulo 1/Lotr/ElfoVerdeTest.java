
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    
    @Test
    public void elfoVerdeNasceComStatusRecemCriado(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Abc");
        assertEquals(Status.RECEM_CRIADO, novoElfoVerde.getStatus());
       
    }
    
    @Test
    public void InventarioTemFlecha(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Abc");
        assertEquals("Flecha", novoElfoVerde.getInventario().buscar("Flecha").getDescricao());
    }
    
    @Test
    public void InventarioTemFlechaArco(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Abc");
        assertEquals("Arco", novoElfoVerde.getInventario().buscar("Arco").getDescricao());
    }
    
    @Test
    public void ganharFlechaDeVidro(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Abc");
        Item flechaDeVidro = new Item(2, "Flecha de Vidro");
        novoElfoVerde.ganharItem(flechaDeVidro);
    }
    
    @Test
    
    public void elfoVerdeAdicionarItemComDescricaoValida(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Abc");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        Inventario inventario = novoElfoVerde.getInventario();
        novoElfoVerde.ganharItem(arcoDeVidro);
        assertEquals(new Item(2, "Flecha"), inventario.obterItem(0));
        assertEquals(new Item(1, "Arco"), inventario.obterItem(1));
        assertEquals(arcoDeVidro, inventario.obterItem(2));
    }
    
    
    
  
   
    
}
