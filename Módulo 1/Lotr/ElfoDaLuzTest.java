

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    @Test
    public void ataca1Vez(){
    ElfoDaLuz novoElfoDaLuz = new ElfoDaLuz("X");
    Dwarf novoDwarf = new Dwarf("y");
    novoElfoDaLuz.atacarComEspada(novoDwarf);
    assertEquals(79.0, novoElfoDaLuz.getQuantidadeDeVida(), 1e-9);
   }
   
   @Test
    public void ataca2Vezes(){
    ElfoDaLuz novoElfoDaLuz = new ElfoDaLuz("X");
    Dwarf novoDwarf = new Dwarf("y");
    novoElfoDaLuz.atacarComEspada(novoDwarf);
    novoElfoDaLuz.atacarComEspada(novoDwarf);
    assertEquals(89.0, novoElfoDaLuz.getQuantidadeDeVida(), 1e-9);
   }
   
    @Test
    public void naoPerdeEspada(){
    ElfoDaLuz novoElfoDaLuz = new ElfoDaLuz("X");
    Inventario inventario = novoElfoDaLuz.getInventario();
    Item espadaG = new Item(1, "Espada de Galvorn");
    Item arco = new Item(1, "Arco");
    novoElfoDaLuz.perderItem(arco);
    novoElfoDaLuz.perderItem(espadaG);
    assertEquals(espadaG, novoElfoDaLuz.inventario.buscar("Espada de Galvorn"));
    assertNull(novoElfoDaLuz.inventario.buscar("Arco"));
   }
   
   
    
   
    
}
