   public class Elfo extends Personagem { 
       private static int qtdElfos;
     
    public Elfo( String nome ) {
        super(nome);
        this.quantidadeDeVida = 100.0;
        this.inventario.adicionarItem(new Item(2, "Flecha"));
        this.inventario.adicionarItem(new Item(1, "Arco"));
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    public Item getFlecha() {
        return inventario.buscar("Flecha");
    }
  
    public int getQuantidadeFlecha() {
        Item flecha = this.getFlecha();
        return flecha.getQuantidade();
    }

    public boolean podeAtirarFlecha() {
        return getQuantidadeFlecha() > 0;
    }

    private void descartarFlecha(int n) {
        if(n > 0) {
            Item flecha = this.getFlecha();
            flecha.setQuantidade(flecha.getQuantidade() - 1);
        }
    }

    public void atirarFlechaNoDwarf(Dwarf dwarf){
        if(podeAtirarFlecha()) {
            Item flecha = this.getFlecha();
            flecha.setQuantidade( this.getQuantidadeFlecha() - 1 );
            this.aumentarXp();
            dwarf.sofrerDano();
            this.sofrerDano();
            
        }
    }

}