import java.util.*;
public class ElfoDaLuz extends Elfo {
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
        "Espada de Galvorn"
        )
    );
    private boolean ataqueImpar;
    
    public ElfoDaLuz( String nome ){
        super(nome);
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
        this.qtdDano = 21;
        this.vidaGanha = 10;
        
        
    }
    {
        ataqueImpar = true;
    }
    
    public void atacarComEspada(Dwarf dwarf){
    if(ataqueImpar && podeSofrerDano()){
        this.aumentarXp();
        this.sofrerDano();
        dwarf.sofrerDano();
        ataqueImpar = false;
    } else {
        this.aumentarXp();
        dwarf.sofrerDano();
        this.ganharVida();
        ataqueImpar = true;
    }
}
    
@Override
    public void perderItem(Item item){
        if(this.inventario.buscar("Espada de Galvorn") != this.inventario.buscar(item.getDescricao())){
        this.inventario.removerItem(item);
    }
    }


    
    
    
}
