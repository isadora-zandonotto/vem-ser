

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {
  
    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionarItem(espada);
        assertEquals(espada, inventario.buscar("Espada"));
    }
    
    @Test
    public void adicionarDoisItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(espada, inventario.buscar("Espada"));
        assertEquals(escudo, inventario.buscar("Escudo"));
    }
    
    @Test
    public void obterItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionarItem(espada);
        assertEquals(espada, inventario.obterItem(0));
        
    }
    
    @Test
    public void removerItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        inventario.removerItem(espada);
        assertNull(inventario.buscar("Espada"));
    }
    
    @Test
    
    public void buscarItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(escudo, inventario.buscar("Escudo"));
    }
    
    @Test
    
    public void pegarDescricoesItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        //inventario.getDescricoesItens();
        assertEquals("Espada, Escudo", inventario.getDescricoesItens());
    }
    
    @Test
    
    public void retornarItemComMaiorQuantidade(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(espada, inventario.retornarItemComMaiorQuantidade());
    }
    
   
    @Test 
    
    public void inverterListaDeUmItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionarItem(espada);
        assertEquals(espada, inventario.inverter().get(0));
        assertEquals(1, inventario.inverter().size());
    } 
    
    @Test 
    
    public void inverterListaDeDoisItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(espada, inventario.inverter().get(1));
        assertEquals(escudo, inventario.inverter().get(0));
        assertEquals(2, inventario.inverter().size());
    } 
    
    @Test
    
    public void getDescricoesNenhumItem(){
        Inventario inventario = new Inventario();
        assertEquals("", inventario.getDescricoesItens());
    }
    
    
    
    
    
    
    
}
