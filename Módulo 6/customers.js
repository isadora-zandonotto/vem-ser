db.createCollection("customers")
custArray = [{ nome: "Fernando", idade: 29 }, { nome: "Teste", "uf": "RS" }]
db.customers.insert(custArray)
db.customers.find().pretty()
db.customers.find({uf: "RS"})
db.customers.find({nome: { $regex: /F/ }})
db.customers.find({idade: {$gte: 18}})
db.customers.find({nome: "Fernando", idade: {$gte: 18}})
db.customers.find({nome: { $regex: /a/ }, idade: {$gte: 18}})
db.customers.update({nome: "Ferando"}, {nome: "Fernando", idade: 29, uf: "RS"})
db.customers.updateOne({_id: ObjectId("5e4ac524711f98ad1cf11c8b")}, {$set: {idade: 28}})
db.customers.updateOne({nome: "Fernando"}, {$set: {uf: "RS"}}, {upsert: true})
db.customers.deleteOne({nome: "Fernando"})

