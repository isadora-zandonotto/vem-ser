db.Turismo.insert(
  {
  nome: "Xangri-lá",
  fundacao: new Date("1992-03-26"),
  distanciaAteACapitalRSKm: 132,
  pontosTuristicos: [
    {
      nome: "Praia de Atlântida",
      descricao: "Atlântida é uma praia brasileira que compõe a orla do município de Xangri-lá.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    },
    {
      nome: "Praia de Xangri-lá",
      descricao: "Xangri-lá é uma praia brasileira do estado do Rio Grande do Sul.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    }
  ]
})

db.Turismo.insert({
  nome: "Gramado",
  fundacao: new Date("1954-12-15"),
  distanciaAteACapitalKm: 95,
  pontosTuristicos: [
    {
      nome: "Lago Negro",
      descricao: "O Lago Negro é um lago artificial e oferece passeio de pedalinhos, bar, restaurante e loja de conveniências.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    },
    {
      nome: "Mini Mundo",
      descricao: "Mini Mundo é um parque em miniatura brasileiro, localizado na cidade de Gramado, no Rio Grande do Sul, sendo uma das mais tradicionais atrações turísticas desta cidade.",
      custoEntrada: 42.0,
      horarioFuncionamento: "Fechado até o dia 20/02 para realização de melhorias no parque."
    }
  ]
})

db.Turismo.insert({
  nome: "Pelotas",
  fundacao: new Date("1758-06-18"),
  distanciaAteACapitalRSKm: 261,
  pontosTuristicos: [
    {
      nome: "Museu de História Natural Carlos Ritter",
      descricao: "O Museu de História Natural Carlos Ritter é um museu brasileiro, localizado em Pelotas, no Rio Grande do Sul. É um órgão do Instituto de Biologia da Universidade Federal de Pelotas e está localizado na Praça Coronel Pedro Osório, Casarão 1.",
      custoEntrada: 0,
      horarioFuncionamento: "De terças a domingos das 10h às 18h."
    },
    {
      nome: "Museu do Doce",
      descricao: "O Museu do Doce da Universidade Federal de Pelotas – situado na Praça Coronel Pedro Osório, número 8 – foi criado em 30 de dezembro de 2011. Configura-se como órgão suplementar do Instituto de Ciências Humanas da UFPel e tem  como missão salvaguardar os suportes de memória da tradição doceira de Pelotas e da região e como  compromisso, produzir conhecimento sobre esse patrimônio.",
      custoEntrada: 0,
      horarioFuncionamento: "De terças a domingos das 14h  as 18h30."
    }
  ]
})

db.Turismo.insert({
  nome: "Toronto",
  fundacao: new Date("1834-03-06"),
  distanciaAteACapitalRSKm: 8.676,
  pontosTuristicos: [
    {
      nome: "CN Tower",
      descricao: "A Torre CN, localizada em Toronto, Ontario, Canadá é uma torre turística e de comunicações que tem 553,33 metros de altura, sendo a terceira maior torre do mundo.",
      custoEntrada: 38,
      horarioFuncionamento: "9h às 22h30"
    },
    {
      nome: "Museu Real de Ontário",
      descricao: "O Museu Real de Ontário está localizado em Toronto, antiga capital do Canadá. Este museu começou a ser construído na Província de Ontário, em 16 de Abril de 1912, abrindo dois anos mais tarde, em 1914. Devido à cada vez maior colecção o Museu Real de Ontário teve de ser expandido três vezes. A primeira vez em 1933, a segunda em 1978 e a terceira em 2005. Desta última, o arquiteto foi o famoso Daniel Libeskind, um arquitecto estadunidense nascido em 1946.",
      custoEntrada: 23,
      horarioFuncionamento: "10h às 17h30"
    }
  ]
})

db.Turismo.insert({
  nome: "Bangkok",
  fundacao: new Date("1782-04-21"),
  distanciaAteACapitalRSKm: 16.587,
  pontosTuristicos: [
    {
      nome: "Wat Arun",
      descricao: "Wat Arun é um wat da escola teravada da cidade de Banguecoque, capital da Tailândia. Está localizado na área de Thonburi, distrito de Bangkok Yai, na margem oeste do rio Chao Phraya. É um dos ícones mais conhecidos da Tailândia.",
      custoEntrada: 100,
      horarioFuncionamento: "8h30 às 17h30"
    },
    {
      nome: "Buda de Ouro",
      descricao: "O Buda de Ouro, cujo nome oficial em tailandês é Phra Maha Suwan Phuttha Patimakon, é a maior estátua de ouro maciço no mundo e um dos mais preciosos tesouros da Tailândia e do budismo.",
      custoEntrada: 40,
      horarioFuncionamento: "8h às 17h"
    }
  ]
})

db.Turismo.insert({
  nome: "São Paulo",
  fundacao: new Date("1554-01-25"),
  distanciaAteACapitalRSKm: 1.135,
  pontosTuristicos: [
    {
      nome: "MASP",
      descricao: "Museu de Arte de São Paulo Assis Chateaubriand é uma das mais importantes instituições culturais brasileiras. Localiza-se, desde 7 de novembro de 1968, na Avenida Paulista, cidade de São Paulo, em um edifício projetado pela arquiteta ítalo-brasileira Lina Bo Bardi para ser sua sede.",
      custoEntrada: 45,
      horarioFuncionamento: "de terças a domingos, das 10h às 18h30."
    },
    {
      nome: "Vila Madalena",
      descricao: "O boêmio bairro da Vila Madalena, muitas vezes chamado simplesmente Vila, conta com lojas de moda independente, cafés casuais e galerias de arte contemporânea. Há um cenário vibrante de arte de rua em toda a área, especialmente no Beco do Batman, um local que se tornou uma galeria ao ar livre para estênceis e murais coloridos.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    }
  ]
})

db.Turismo.updateOne( {nome: "Gramado"}, {$push: {pontosTuristicos: {nome: "Lago Negro", descricao: "O Lago Negro é um lago artificial e oferece passeio de pedalinhos, bar, restaurante e loja de conveniências.", custoEntrada: 0, horarioFuncionamento: "Sempre aberto"}}} )
db.Turismo.update( { nome: "Gramado"}, {$pull: {"pontosTuristicos" : { nome : "Mini Mundo" } }})
db.Turismo.updateOne( {nome: "São Paulo"}, {$set: {distanciaAteACapitalKm: 1136}})
db.Turismo.find( { "pontosTuristicos.custoEntrada": { $gte : 10.00 }, "distanciaAteACapitalKm" : 1136 } )
